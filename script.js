var button = document.getElementById("enter");
var input = document.getElementById("userinput");
var ul = document.querySelector("ul");
function inputLength() {
	return input.value.length;
}

function Delete(li){
li.parentNode.parentNode.removeChild(li.parentNode);
}

function done() {
	var li = event.target;
	li.classList.toggle('done');
}

function createListElement(event) {
	var li = document.createElement("li");
	
	//li.appendChild(document.createTextNode(input.value));
    var text = input.value;
	if ( text !== '' ) {
 
 
	li.addEventListener('click', function(){ done();}, false);	
	li.innerHTML += '<li>' + text + " " + '<button onclick="Delete(this);">x</button> </li>';
    li.classList.add('nobull');
	ul.appendChild(li);
	input.value = "";
	}
	event.preventDefault();
}

function addListAfterClick(event) {
	if (inputLength() > 0) {
		createListElement(event);
	}
}

function addListAfterKeypress(event) {
	if (inputLength() > 0 && event.keyCode === 13) {
		createListElement();
	}
}


button.addEventListener("click", addListAfterClick);
input.addEventListener("keypress", addListAfterKeypress);